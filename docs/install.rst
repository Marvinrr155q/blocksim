.. Licensed under the MIT Licensed

.. _install:

============
Installation
============

.. highlight:: console

.. _blocksim_pypi: https://pypi.org/project/blocksim/
.. _setuptools: https://pypi.org/project/setuptools/


You can install blocksim in the usual ways. The simplest way is with pip::

    $ pip install blocksim

Checking the installation
-------------------------

If all went well, you should be able to open a command prompt, and see
blocksim installed properly:

.. parsed-literal::

    $ python -m blocksim --version
    blocksim, version |release|
    Documentation at |doc-url|
