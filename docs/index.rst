.. blocksim documentation master file, created by
   sphinx-quickstart on Mon Jan  6 15:10:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Content
=======

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: User Manual

    install
    modules
    examples/index

Indices and tables
==================

* :ref:`genindex`
