import numpy as np


# Constantes
Req = 6378137
Rpo = 6356752.3
rf = 298.257223563
omega = 7.2921151467064e-5
kb = 1.380649e-23
mu = 3.986004418e14
c = 299792458
jour_sideral = 2 * np.pi / omega
jour_stellaire = 86400
J2 = 1.08263e-3
