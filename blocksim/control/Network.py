from typing import Iterable
from itertools import product
from collections import OrderedDict

import numpy as np

from ..core.Frame import Frame
from ..core.Node import AComputer


# Client / Server
